# one-page

#### [https://pragalakis.gitlab.io/one-page/](https://pragalakis.gitlab.io/one-page/)

![screenshot](screenshot.png)

## What's inside?

- Choo - a frontend framework
- Browserify - a bundler
- Budo - a dev server
- Sheetify - a css bundler

## run it

- Clone this repo
- Remove the `.git` folder
- `npm install`
- `npm start`

## build it

- `npm run build`

This ll create a `app.js` file and copy the `index.html` file and all assets on `./build` directory

## no-js

for a non-javascript version (just html+css) go to --> [link](https://gitlab.com/pragalakis/one-page/tree/no-js)
