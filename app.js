const html = require('choo/html');
const choo = require('choo');
const css = require('sheetify');

const app = choo();
app.route('/', mainView);
app.route('/one-page', mainView);
app.mount('#app');

function mainView(state, emit) {
  const scroll = e => {
    e.preventDefault();
    const element = document.getElementById(e.target.innerHTML);
    if (element) element.scrollIntoView({ behavior: 'smooth', block: 'start' });
  };

  return html`
    <body class=${style}>
      <h1 class="title">agkmrvwx</h1>
      <nav class="navigation">
        <li class="left top">
          <a href="/" onclick=${scroll}>about</a>
        </li>
        <li class="right top">
          <a href="/" onclick=${scroll}>contact</a>
        </li>
        <li style="margin-left:-35px" class="left mid">
          <a href="/" onclick=${scroll}>location</a>
        </li>
        <li style="margin-right:-35px" class="right mid">
          <a href="/" onclick=${scroll}>participate</a>
        </li>
        <li class="left bottom">
          <a href="/" onclick=${scroll}>events</a>
        </li>
        <li class="right bottom">
          <a href="/" onclick=${scroll}>listen</a>
        </li>
      </nav>
      <div id="top" class="container">
        <div id="top">
          <img class="tape" src='./assets/tape.png' as='tape'/>
        </div>
        <div id="about">
          <h2>About</h2>
          <p>
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
            proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
            proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
          </p>
          <p>
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
            proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
          </p>
        </div>
        <div id="contact">
          <h2>Contact</h2>
          <img src='./assets/peter-hershey-132725-unsplash.png' width='100%' as='contact-img'/>
          <p>
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
            proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
          </p>
        </div>
        <div id="location">
          <h2>Location</h2>
          <img src='./assets/thr3-eyes-173627-unsplash.png' width='100%' as='location-img'/>
          <p>
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
            proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
          </p>
        </div>
        <div id="participate">
          <h2>Participate</h2>
          <p>
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
            proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
          </p>
        </div>
        <div id="events">
          <h2>Events</h2>
          <ul>
            <li>Quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo</li>
            <li>Nostrud exercitation ullamco</li>
            <li>Cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non</li>
            <li>Deserunt mollit anim id est laborum</li>
          </ul>
        </div>
        <div id="listen">
          <h2>Listen</h2>
          <img src='./assets/jon-tyson-644856-unsplash.png' width='100%' as='listen-img'/>
          <p>
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
            proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
          </p>
        </div>
        <div class="footer">
          <a class="arrow" href="/" onclick=${scroll}>top</a>
          <p>Made with <a href="https://www.choo.io" target="_blank" rel="noopener noreferrer">🚂</a> and ☕</p>
        </div>
      </div>
    </body>
  `;
}

const style = css`
  :host {
    font-family: 'helvetica neue', helvetica, sans-serif;
    font-size: 13px;
    text-align: center;
    background: rgb(252, 165, 241);
    background: linear-gradient(
      180deg,
      rgba(252, 165, 241, 1) 35%,
      rgba(181, 255, 255, 1) 100%
    );
  }

  :host p,
  :host ul {
    line-height: 26px;
    letter-spacing: 1.5px;
  }

  :host a {
    color: #000000;
    text-decoration: none;
  }

  :host a:hover {
    text-decoration: underline;
  }

  :host h2 {
    font-weight: 300;
    letter-spacing: 2px;
  }

  :host .title {
    font-family: 'Major Mono Display';
    font-size: 3rem;
    font-weight: 400;
    text-transform: uppercase;
    position: fixed;
    left: 20%;
    right: 20%;
    margin-top: 15px;
  }

  :host nav.navigation li {
    list-style: none;
    position: fixed;
    font-size: 18px;
    text-transform: capitalize;
  }

  :host nav.navigation li a {
    text-decoration: none;
    color: #000000;
    letter-spacing: 4px;
  }

  :host nav.navigation .left {
    left: 30px;
  }

  :host nav.navigation .right {
    right: 30px;
  }

  :host nav.navigation .top {
    top: 35px;
  }

  :host nav.navigation .mid {
    top: 50%;
    -webkit-transform: rotate(-90deg);
    -moz-transform: rotate(-90deg);
    -ms-transform: rotate(-90deg);
    -o-transform: rotate(-90deg);
  }

  :host nav.navigation .bottom {
    bottom: 35px;
  }

  :host .container {
    max-width: 800px;
    margin: 0 auto;
    padding-left: 20%;
    padding-right: 20%;
  }

  :host .container div:first-child {
    padding-top: 15vh;
  }

  :host .tape {
    width: 50%;
  }

  :host .container div {
    padding-top: 10vh;
  }

  :host .footer {
    padding-top: 5vh;
  }

  :host .arrow::before {
    width: 0;
    height: 0;
    border-left: 5px solid transparent;
    border-right: 5px solid transparent;
    border-bottom: 5px solid black;
  }

  :host ul {
    max-width: 400px;
    margin: 0 auto;
    padding-left: 0px;
    list-style-type: square;
    list-style-position: inside;
  }

  @media only screen and (max-width: 800px) {
    :host nav.navigation {
      display: none;
    }

    :host .container {
      padding-left: 10%;
      padding-right: 10%;
    }

    :host .title {
      font-size: 2rem;
    }
  }
`;
